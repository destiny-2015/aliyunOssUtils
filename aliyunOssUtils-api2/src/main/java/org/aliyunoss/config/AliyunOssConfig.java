package org.aliyunoss.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "aliyun.oss")
@Data
@Configuration
public class AliyunOssConfig {
    private String endpoint;

    private String bucket;

    private String accessKey;

    private String secretKey;

    //private String marketHost;


    @Bean
    //@Scope(value = "prototype")
    public OSS ossClient() {
        return new OSSClientBuilder().build(endpoint, accessKey, secretKey);
    }
    public static OSS createOss(AliyunOssConfig config){
        return new OSSClientBuilder().build(config.getEndpoint(), config.getAccessKey(), config.getSecretKey());
    }
}