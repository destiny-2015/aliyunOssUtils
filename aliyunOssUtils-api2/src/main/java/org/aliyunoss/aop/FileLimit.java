package org.aliyunoss.aop;

import org.aliyunoss.utils.FileLimitUnit;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;
/**
 * @Description ：FileLimit 注解，内置参数value，max，以及文件单位
 */
@Documented
@Target(ElementType.METHOD) // 作用与方法上
@Retention(RetentionPolicy.RUNTIME) // RUNTIME: 在运行时有效（即运行时保留）
public @interface FileLimit {
    @AliasFor("max") // @AliasFor 表示其可与max互换别名：当注解指定value时，为max赋值
    int value() default 5;
    // 定义单个文件最大限制
    @AliasFor("value") // @AliasFor 表示其可与value互换别名：当注解指定max是，为value赋值
    int max() default 5;
    // 文件单位，默认定义为MB

    //定义单次上传文件的总大小,默认50MB
    int maxRequestSize() default 50;

    //上传文件格式，默认是图片
    String fileFormat() default "images";
    FileLimitUnit unit() default FileLimitUnit.MB;
}