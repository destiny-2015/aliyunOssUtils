package org.aliyunoss.controller;

import cn.hutool.core.io.FileTypeUtil;
import org.aliyunoss.utils.MyFileUtils;
import org.aliyunoss.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class test {
    @RequestMapping("test")
    public String test() {
        return "HelloWorld";
    }
    @RequestMapping("image")
    public Result uploadImages(@RequestParam("images") List<MultipartFile> multipartFile) {
        String type="";
        for (MultipartFile file : multipartFile) {
            type = FileTypeUtil.getType(MyFileUtils.multipartFileToFile(file));
        }
        return Result.success(type);
    }
}
