package org.aliyunoss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliyunOssUtilsApi2Application {

	public static void main(String[] args) {
		SpringApplication.run(AliyunOssUtilsApi2Application.class, args);
	}

}
