# aliyunOssUtils

#### 介绍
阿里云OSS上传助手，分三个版本，
版本1==>api1:简单上传，多文件使用for循环
版本2==>api2:简单上传，多文件使用for循环，使用自定义注解配合aop切入限制上传文件大小，通过hutool工具类(FileTypeUtil.getType)限制上传文件类型
版本3==>api3:使用多线程上传多文件，使用自定义注解配合aop切入限制上传文件大小，通过hutool工具类(FileTypeUtil.getType)限制上传文件类型


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

