package org.aliyunoss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliyunOssUtilsApi3Application {

	public static void main(String[] args) {
		SpringApplication.run(AliyunOssUtilsApi3Application.class, args);
	}

}
