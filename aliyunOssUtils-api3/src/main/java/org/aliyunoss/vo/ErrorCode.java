package org.aliyunoss.vo;

public enum  ErrorCode {

    PARAMS_NULL(400,"用户名或密码为空"),
    PARAMS_ERROR(10001,"参数有误"),

    ACCOUNT_PWD_NOT_EXIST(10002,"用户名或密码不存在"),
    TOKEN_ILLEGAL(1003,"token不合法"),
    NO_PERMISSION(70001,"无访问权限"),
    SESSION_TIME_OUT(90001,"会话超时"),
    NO_LOGIN(90002,"未登录"),
    ACCOUNT_EXIST(90003, "账号已存在"),
    NULL_IMAGES(80001,"图片集合为空"),
    IMAGES_FORMAT_ILLEGAL(80002,"图片格式违法"),
    NULL_VIDEO(70001,"视频为空"),

    FILE_DATA_EXCEPTION(60001,"文件数据异常");

    private int code;
    private String msg;

    ErrorCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
