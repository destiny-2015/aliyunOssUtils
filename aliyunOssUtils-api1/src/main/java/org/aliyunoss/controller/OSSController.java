package org.aliyunoss.controller;

import org.aliyunoss.aop.FileLimit;
import org.aliyunoss.service.OssService;
import org.aliyunoss.utils.FileLimitUnit;
import org.aliyunoss.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/aliyunoss")
//@Api("阿里云oss云存储控制器")

@Slf4j
public class OSSController {

    @Autowired
    private OssService ossService;

    //@PostMapping("/uploadImages")
    //@FileLimit(value = 30, maxRequestSize = 35, unit = FileLimitUnit.MB)
    ////@ApiOperation("多图片上传")
    //public Result uploadImages(@RequestParam("images") List<MultipartFile> multipartFile) {
    //
    //    // 图片校验
    //    //for (MultipartFile file : multipartFile) {
    //    //    // 图片为空
    //    //    if (file.isEmpty()) {
    //    //        return Result.fail(ErrorCode.NULL_IMAGES.getCode(), "请选择最少一张图片上传");
    //    //    }
    //    //    // 判断是否为图片，防止恶意使用
    //    //    File multipartFileToFile = MyFileUtils.multipartFileToFile(file);
    //    //    if (!MyFileUtils.isImage(multipartFileToFile)) {
    //    //        return Result.fail(ErrorCode.IMAGES_FORMAT_ILLEGAL.getCode(), "只支持bmp/gif/jpg/jfif/jpeg/png/webp格式");
    //    //    }
    //    //}
    //    // 文件上传，获取上传得到的图片地址返回
    //    List<String> responseUrls = ossService.uploadImages(multipartFile);
    //
    //    return Result.success(responseUrls);
    //}

    @PostMapping("/uploadByThreads")
    @FileLimit(value = 33, maxRequestSize = 1000,unit = FileLimitUnit.MB)
    public Result upload(@RequestParam("images") List<MultipartFile> multipartFile) {

        //// 图片校验
        //for (MultipartFile file : multipartFile) {
        //    // 图片为空
        //    if (file.isEmpty()) {
        //        return Result.fail(100004,"请选择最少一张图片上传");
        //    }
        //    // 判断是否为图片，防止恶意使用
        //    File multipartFileToFile = MyFileUtils.multipartFileToFile(file);
        //    if (!MyFileUtils.isImage(multipartFileToFile)) {
        //        return Result.fail(100005,"只支持bmp/gif/jpg/jfif/jpeg/png/webp格式");
        //    }
        //}
        // 文件上传，获取上传得到的图片地址返回
        List<String> responseUrls = ossService.upload(multipartFile);
        return Result.success(responseUrls);
    }

    //上传视频
    //@PostMapping("/fileUploadZone")
    //@FileLimit(value = 1024, fileFormat = "videos", unit = FileLimitUnit.MB)//单次文件最大1Gb
    //public Result fileUploadZone(@RequestParam("videos") MultipartFile multipartFile) {
    //    //    视频格式校验
    //    //    if (!file.isEmpty()) {
    //    //        return Result.fail(ErrorCode.NULL_VIDEO.getCode(), "请选择需要上传的视频");
    //    //    }
    //    //    if (!MyFileUtils.isVideo(file)){
    //    //        return Result.fail(ErrorCode.IMAGES_FORMAT_ILLEGAL.getCode(), "仅支持.mp4 .avi .wmv .mpg .mpeg .mpv .rm .ram .swf .flv .mov .qt .navi 格式类型视频");
    //    //    }
    //    return ossService.fileUploadZone(multipartFile);
    //}
    //
    ////    批量上传视频
    //@PostMapping("/filesUploadZone")
    //@FileLimit(value = 100, fileFormat = "videos", maxRequestSize = 500,unit = FileLimitUnit.MB)//单次文件最大1Gb
    //public Result filesUploadZone(@RequestParam("videos") List<MultipartFile> multipartFile) {
    //    //    视频格式校验
    //    //    if (!file.isEmpty()) {
    //    //        return Result.fail(ErrorCode.NULL_VIDEO.getCode(), "请选择需要上传的视频");
    //    //    }
    //    //    if (!MyFileUtils.isVideo(file)){
    //    //        return Result.f
    //    //        ail(ErrorCode.IMAGES_FORMAT_ILLEGAL.getCode(), "仅支持.mp4 .avi .wmv .mpg .mpeg .mpv .rm .ram .swf .flv .mov .qt .navi 格式类型视频");
    //    //    }
    //    Result result = ossService.filesUploadZone(multipartFile);
    //    return result;
    //}

}

