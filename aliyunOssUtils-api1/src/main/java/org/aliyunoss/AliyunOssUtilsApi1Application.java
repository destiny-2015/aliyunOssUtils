package org.aliyunoss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AliyunOssUtilsApi1Application {

	public static void main(String[] args) {
		SpringApplication.run(AliyunOssUtilsApi1Application.class, args);
	}

}
