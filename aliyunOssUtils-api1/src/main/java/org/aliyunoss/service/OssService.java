package org.aliyunoss.service;

import org.aliyunoss.vo.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface OssService {
    //单张图片上传
    //String uploadPicture(MultipartFile file, String fileName,String objectKey);
    List<String> upload(List<MultipartFile> multipartFile);
    //多图上传
    List<String> uploadImages(List<MultipartFile> multipartFile);
    //String uploadpictures(MultipartFile[] file, String fileName,String objectKey);
    //String uploadFileByName(String fileName);

//    分片上传视频文件
    Result fileUploadZone(MultipartFile multipartFile);
    Result filesUploadZone(List<MultipartFile> multipartFile);
}
