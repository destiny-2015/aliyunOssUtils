package org.aliyunoss.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author YK Fei
 * @Date 2023/1/4 10:15
 * @MethodName
 * @Param
 * @Return
 * 实现文件名长度和名称的限制
 */
public class MyFileUtils {
    /**
     * @param imageFile 需要判断的文件
     * @return boolean
     * @description 判断文件是否为图片
     * @method isImage
     **/
    public static boolean isImage(File imageFile) {
        if (!imageFile.exists()) {
            return false;
        }
        Image img = null;
        try {
            img = ImageIO.read(imageFile);
            return img != null && img.getWidth(null) > 0 && img.getHeight(null) > 0;
        } catch (Exception e) {
            return false;
        } finally {
            // 最终重置为空
            img = null;
            imageFile.delete();
        }
    }
    /**
     * @Author YK Fei
     * @Date 2023/1/5 15:52
     * @MethodName isVideo
     * @Param [multipartFile]
     * @Return boolean
     * @Description 判断文件是否为视频格式（.mp4 .avi .wmv .mpg .mpeg .mpv .rm .ram .swf .flv .mov .qt .navi）
     */
    public static boolean isVideo(MultipartFile multipartFile){

        List<String> formatList = new ArrayList<>();

        formatList.add("avi");
        formatList.add("flv");
        formatList.add("mov");
        formatList.add("mp4");
        formatList.add("mpg");
        formatList.add("mpeg");
        formatList.add("mpv");
        formatList.add("navi");
        formatList.add("qt");
        formatList.add("rm");
        formatList.add("ram");
        formatList.add("ram");
        formatList.add("ram");
        formatList.add("swf");
        formatList.add("wmv");



        String originalFilename = multipartFile.getOriginalFilename();
        String format = StringUtils.substringAfterLast(originalFilename,".");
        for (int i = 0; i < formatList.size(); i++) {
            if (format.equalsIgnoreCase(formatList.get(i))){
                return true;
            }
        }

        return true;
    }


    /**
     * @return java.io.File
     * @description MultipartFile转File
     * @method multipartFileToFile
     **/
    public  static File multipartFileToFile(MultipartFile multipartFile) {
        if (multipartFile.isEmpty()) {
            return null;
        }
        InputStream inputStream = null;
        try {
            inputStream = multipartFile.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File file = new File(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        try {
            OutputStream os = Files.newOutputStream(file.toPath());
            int bytesRead;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    /**
     * @param filename 文件名
     * @description 获取文件拓展名
     * @method getExtensionName
     **/
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot);
            }
        }
        return filename;
    }


    /**
     * Description: 判断OSS服务文件上传时文件的contentType
     * @param filenameExtension 文件后缀
     * @return String
     */
    public static String getContentType(String filenameExtension) {
        String contentType = "";
        switch(filenameExtension.toUpperCase()) {
            //image contentType
            case "BMP": contentType = "image/bmp";break;
            case "GIF": contentType = "image/gif";break;
            case "JPEG":
            case "JPG":contentType = "image/jpg";break;
            case "ICO": contentType="image/x-icon";break;
            case "TIF":
            case "TIFF": contentType="image/tiff";break;
            case "PNG": contentType = "image/png";break;
            case "WBMP": contentType = "image/vnd.wap.wbmp";break;
            case "WEBP": contentType = "image/webp";break;
            case "JFIF": contentType = "image/jpeg";break;
            //video contentType
            case "AVI": contentType = "video/avi";break;
            case "FLV": contentType = "video/x-flv";break;
            case "MP4": contentType = "video/mpeg4";break;
            case "MPEG": contentType = "video/mpg";break;
            case "WMV": contentType = "video/x-ms-wmv";break;
            case "WMA": contentType = "video/wma";break;
            case "W4A": contentType = "video/mp4";break;
            case "W4V": contentType = "video/mp4";break;
            case "WOV": contentType = "video/quicktime";break;
            case "3GP": contentType = "video/3gpp";break;
            case "WEBM": contentType = "video/webm";break;
            case "VOB": contentType = "video/vob";break;
            case "MKV": contentType = "video/x-matroska";break;

            case "HTML": contentType = "text/html";break;
            case "TXT": contentType = "text/plain";break;
            case "VSD": contentType = "application/vnd.visio";break;
            case "PPTX":
            case "PPT": contentType = "application/vnd.ms-powerpoint";break;
            case "DOCX": contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";break;
            case "DOC": contentType = "application/msword";break;
            case "XLSX": contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";break;
            case "XLS": contentType = "application/vnd.ms-excel";break;
            case "XML": contentType = "text/xml";break;
            case "PDF": contentType = "application/pdf";break;


            default:contentType="file";
        }
        return contentType;
    }

}
