package org.aliyunoss.utils;

public enum FileLimitUnit {
    KB, MB, GB
}